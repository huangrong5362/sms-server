/**
 * Created by yaojiewei on 2017/6/7.
 */
let Func = {
    BaseUrl: 'http://192.168.1.104:',

    // 校验邮箱
    validateMail: function (mail) {
        let myreg2 = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        return myreg2.test(mail) ? true : false;
    },

    // 校验手机号
    validateMobile: function (mobile) {
        let myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        return myreg.test(mobile) ? true : false;
    },


};
package com.hthj.smart.gsm.modem.controller;

import java.util.Date;
import java.util.List;

import javax.jws.WebParam;

import org.smslib.InboundMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hthj.smart.gsm.modem.activitor.SmsHandler;
import com.hthj.smart.gsm.modem.entity.Messages;
import com.hthj.smart.gsm.modem.entity.ReceiveMsg;
import com.hthj.smart.gsm.modem.service.IReceiveMsgService;

/**
 * 短信猫服务的controller层
 * 
 * @author wudi
 *
 */
@Controller
@RequestMapping(value = "ReceiveMsg")
public class ReceiveMsgController {

	@Autowired
	private IReceiveMsgService receiveMsgService;
	
	@Autowired
	private SmsHandler smsHandler;

	public void setSmsHandler(SmsHandler smsHandler) {
		this.smsHandler = smsHandler;
	}

	public void setReceiveMsgService(IReceiveMsgService receiveMsgService) {
		this.receiveMsgService = receiveMsgService;
	}

	private ReceiveMsgController() {

	}
	
	public Boolean receiveMsg(){
		ReceiveMsg msg = new ReceiveMsg();
		List<InboundMessage> readList = smsHandler.readSMS();
		if(!readList.isEmpty()){
			String phones = null;
			String content = null;
			for(InboundMessage in:readList){
				phones = in.getOriginator();
				content = in.getText();
				Date time = in.getDate();
				msg.setPhones(phones);
				msg.setContent(content);
				msg.setTime(time);
				receiveMsgService.save(msg);
			}
		}
		return true;
		
	}

	/**
	 * 删除短信
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "deleteReceiveMsg", method = RequestMethod.POST)
	public Boolean delete(@RequestBody List<String> ids) {
		return receiveMsgService.delete(ids);

	}

	/**
	 * 修改短信内容
	 * 
	 * @param msg
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Boolean update(@RequestBody ReceiveMsg msg) {
		return receiveMsgService.update(msg);

	}

	/**
	 * 查所有信息
	 */
	@ResponseBody
	@RequestMapping(value = "findAll", method = RequestMethod.GET)
	public List<ReceiveMsg> findAll() {
		return receiveMsgService.queryAll();
	}

	/**
	 * 根据Id查详情
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryById", method = RequestMethod.GET)
	public ReceiveMsg queryById(@WebParam String id) {
		return receiveMsgService.queryById(id);
	}

	/**
	 * 条件查询
	 * 
	 * @param phone
	 * @param key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryAll", method = RequestMethod.GET)
	public List<ReceiveMsg> queryAll(@WebParam String phone, String key,String startTime,String endTime) {
		return receiveMsgService.queryBy(phone, key,startTime,endTime);
	}
}

package com.hthj.smart.gsm.modem.controller;

import java.util.List;

import javax.jws.WebParam;

import org.apache.log4j.Logger;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hthj.smart.gsm.modem.activitor.SmsHandler;
import com.hthj.smart.gsm.modem.config.Config;
import com.hthj.smart.gsm.modem.entity.Messages;
import com.hthj.smart.gsm.modem.service.IMessageService;

/**
 * 短信猫服务的controller层
 * 
 * @author wudi
 *
 */
@Controller
@RequestMapping(value = "msg")
public class MessagesController {

	private static Logger logger = Logger.getLogger("com/hthj/smart/gsm/modem/controller/MessagesController");
	@Autowired
	private SmsHandler smsHandler;
	@Autowired
	private IMessageService messageService;

	public void setMessageService(IMessageService messageService) {
		this.messageService = messageService;
	}

	public void setSmsHandler(SmsHandler smsHandler) {
		this.smsHandler = smsHandler;
	}

	private MessagesController() {

	}

	/**
	 * 发送并保存短信
	 * 
	 * @param appCode
	 *            应用名称
	 * @param phones
	 *            手机号
	 * @param content
	 *            短信内容
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "sendMsg", method = RequestMethod.POST)
	public synchronized Boolean send(@RequestBody String appCode, String phones, String content) {
		if (appCode.isEmpty() || appCode.equals(Config.appCodes)) {
			return false;
		}
		try {
			SmsHandler.getInstance().sycInit();
			String[] phone = null;
			Boolean group = null;
			if (phones.contains(",")) {
				phone = phones.split(",");
				group = true;
			} else {
				phone[0] = phones;
				group = false;
			}
			for (String p : phone) {
				Messages msg = new Messages();
				OutboundMessage outMsg = new OutboundMessage(p, content); // outboundMessage的两个参数：手机号、短信内容
				smsHandler.send(p, content);
				Boolean result = Service.getInstance().sendMessage(outMsg);

				msg.setAppCode(appCode);
				msg.setContent(content);
				msg.setGroup(group);
				msg.setPhones(phones);
				msg.setResult(result);
				messageService.save(msg);

				return result;
			}
			SmsHandler.getInstance().destroy();

		} catch (Exception e) {
			logger.error("send error:", e);
			return false;
		}
		return null;
	}

	/**
	 * 删除短信
	 * 
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "deleteMsg", method = RequestMethod.POST)
	public Boolean delete(@RequestBody List<String> ids) {
		return messageService.delete(ids);

	}

	/**
	 * 修改短信内容
	 * 
	 * @param msg
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Boolean update(@RequestBody Messages msg) {
		return messageService.update(msg);

	}

	/**
	 * 查所有信息
	 */
	@ResponseBody
	@RequestMapping(value = "findAll", method = RequestMethod.GET)
	public List<Messages> findAll() {
		return messageService.queryAll();
	}

	/**
	 * 根据Id查详情
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryById", method = RequestMethod.GET)
	public Messages queryById(@WebParam String id) {
		return messageService.queryById(id);
	}

	/**
	 * 条件查询
	 * 
	 * @param appCode
	 * @param phone
	 * @param key
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryAll", method = RequestMethod.GET)
	public List<Messages> queryAll(@WebParam String appCode, String phone, String key,String startTime,String endTime) {
		return messageService.queryBy(appCode, phone, key,startTime,endTime);
	}
}

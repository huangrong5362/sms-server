package com.hthj.smart.gsm.modem.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hthj.smart.gsm.modem.entity.ReceiveMsg;

public interface ReceiveMsgDAO extends JpaRepository<ReceiveMsg, Serializable>{

	/**
	 * 条件查询
	 * @param phone
	 * @param content
	 * @return
	 */
	@Query("from ReceiveMsg m where 1=1 "
			+ "and( (?2 is null) or ((?2 is not null) and m.phones like ?2))"
			+ "and( (?3 is null) or ((?3 is not null) and m.content like ?3))"
			+ "and( m.time >= ?4 and m.time <= ?5 )")
	public List<ReceiveMsg> findByKey(String phone, String content,Date startTime,Date endTime);
	
	@Query("from ReceiveMsg m where 1=1 "
			+ "and( (?2 is null) or ((?2 is not null) and m.phones like ?2))"
			+ "and( (?3 is null) or ((?3 is not null) and m.content like ?3))")
	public List<ReceiveMsg> findBy(String phone, String content);
	
	/* 
	 * 按时间排序
	 */
	@Query("from ReceiveMsg m where 1=1 order by m.time desc")
	public List<ReceiveMsg> findAll();
	
}
package com.hthj.smart.gsm.modem.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hthj.smart.gsm.modem.entity.Messages;

public interface MessagesDAO extends JpaRepository<Messages, Serializable>{

	/**
	 * 条件查询
	 * @param appCode
	 * @param phone
	 * @param content
	 * @return
	 */
	@Query("from Messages m where 1=1 "
			+ "and( (?1 is null) or ((?1 is not null) and m.appCode like ?1))"
			+ "and( (?2 is null) or ((?2 is not null) and m.phones like ?2))"
			+ "and( (?3 is null) or ((?3 is not null) and m.content like ?3))"
			+ "and( m.time >= ?4 and m.time <= ?5 )")
	public List<Messages> findByKey(String appCode,String phone, String content,Date startTime,Date endTime);
	
	@Query("from Messages m where 1=1 "
			+ "and( (?1 is null) or ((?1 is not null) and m.appCode like ?1))"
			+ "and( (?2 is null) or ((?2 is not null) and m.phones like ?2))"
			+ "and( (?3 is null) or ((?3 is not null) and m.content like ?3))")
	public List<Messages> findBy(String appCode,String phone, String content);
	/* 
	 * 按时间排序
	 */
	@Query("from Messages m where 1=1 order by m.time desc")
	public List<Messages> findAll();
	
}

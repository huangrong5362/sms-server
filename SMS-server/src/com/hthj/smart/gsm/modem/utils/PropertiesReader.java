package com.hthj.smart.gsm.modem.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//import com.hthj.smart.gsm.modem.utils.PropertiesReader;

public class PropertiesReader {
	private Properties properties = new Properties();
	public PropertiesReader(String configPath){
		this.setProperties(new File(configPath));
	}
	public PropertiesReader(InputStream input){
		this.setProperties(input);
	}
	
	private void setProperties(File file){
		try {
			if(!file.exists()){
				throw new IOException("file not found exception!"+file.getAbsolutePath());
			}
			if(file.isDirectory()){
				File[] files = file.listFiles();
				for(File fPath : files){
			         		setProperties(fPath);
				}
			}else if(file.getName().contains(".properties")){
				properties.load(new FileInputStream(file));
			}
		} catch (FileNotFoundException e) {
			System.out.println("properties file not found");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void setProperties(InputStream fileInput){
		try {
			
				properties.load(fileInput);
		
		} catch (FileNotFoundException e) {
			System.out.println("properties file not found");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String get(String key){
		return this.properties.getProperty(key);
	}
	
	public static void main(String[] args) {
		PropertiesReader pr = new PropertiesReader("./src/config.properties");
		System.out.println(pr.properties.keySet());
	}

}

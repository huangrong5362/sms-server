package com.hthj.smart.gsm.modem.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringBeanGetter implements ApplicationContextAware{
	private static ApplicationContext context;
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		context = arg0;
	}
	public static Object getBean(String name){
		return context.getBean(name);
	}
	
	public static <T> T getBean(Class<T> clazz){
		return context.getBean(clazz);
	}
}

package com.hthj.smart.gsm.modem.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * @类名: CrossDomainFilter
 * @描述: 解决浏览器的跨域问题
 * @版本: 
 * @创建日期: 2014-9-12下午05:04:13
 * @作者: 程特儿
 * @JDK: 1.6
 * 
 */
/*
* 类的横向关系：ajax跨域问题，加入参数。
*/
public class CrossDomainFilter implements Filter {

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		arg1.setCharacterEncoding("UTF-8");
		HttpServletResponse res = (HttpServletResponse) arg1;
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Credentials", "true");
		res.addHeader("Access-Control-Allow-Methods",
				"GET,PUT,POST,DELETE,OPTIONS");
		res.addHeader("Access-Control-Allow-Headers",
				"Content-Type,Account,Password,OldPassword,NewPassword,X-Requested-With,Email");
		arg2.doFilter(arg0, arg1);

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}

package com.hthj.smart.gsm.modem.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.sql.Timestamp;
import java.text.*;

import org.apache.commons.lang3.StringUtils;

/**
 * @类名: DateTimeUtil
 * @描述: 时间日期处理工具类
 * @版本: 
 * @创建日期: 2015-3-18下午02:06:14
 * @作者: 虞结诚
 * @JDK: 1.6
 */
public class DateTimeUtil {

	  //字符串转化成时间类型（字符串可以是任意类型，只要和SimpleDateFormat中的格式一致即可）

		/**  
		* sql.Date型日期转化util.Date型日期  
		* @param p_sqlDate sql.Date型日期  
		* @return java.util.Date util.Date型日期  
		*/  
		public static java.util.Date toUtilDateFromSqlDate( java.sql.Date p_sqlDate ) {   
		   java.util.Date returnDate = null;   
		   if ( p_sqlDate != null ) {   
		    returnDate = new java.util.Date( p_sqlDate.getTime() );   
		   }   
		   return returnDate;   
		}   
		public static String formatDateToString(Date date){
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
			String str=sdf.format(date); 
			return str;
		}
		
		public static String formatDateToStringYMD(Date date){
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");   
			String str=sdf.format(date); 
			return str;
		}

		public static String timestampToDate(long timestamp){
			Timestamp ts = new Timestamp(timestamp);  
			  Date date = new Date(ts.getTime());  
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				String str=sdf.format(date); 
			return str; 
		}
		/**
		 * 
		 * StrToDate
		 * @描述: 把yyyyMMdd时间格式的字符串转化为时间对象。 字符串长度必须一致
		 * @作者: huangr
		 * @创建时间: 2015-3-26下午02:28:18
		 * 
		 * @修改描述: TODO 请描述修改内容
		 * @修改人: huangr
		 * @修改时间: 2015-3-26下午02:28:18
		 * @param time
		 * @return
		 * @throws ParseException
		 */
	  public static Date strToDate(String time) {

	    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
	        "yyyy-MM-dd");

	    java.util.Date d=null;
		try {
			d = sdf.parse(time);
		} catch (ParseException e) {
			d = null;
		}

	    return d;
	  }
	  
	  /**
	   * 字符转日期
	   * @param time 字符表示的日期如"2007-10-17"
	   * @param format 日期格式如"yyyy-MM-dd"  大小写必须一致
	   * @return 日期s
	   * @throws ParseException
	   */
	  public static Date strToDate(String time, String format){
		  if (time == null)
			  return null;
		  try{
			  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(format);
				java.util.Date d = sdf.parse(time);
					return d;  
		  }catch(Exception e){
			  return null;
		  }
		  
	 }
	  /**
	   * 字符转日期（固定格式"yyyyMMdd:HH"）
	   * @param time 字符表示的日期如"20071017:20"
	   * @return  日期加小时
	   * @throws ParseException 
	   */
	  public static Date strToTime(String time) {

	    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
	        "yyyyMMdd:HH");

	    java.util.Date d=null;
		try {
			d = sdf.parse(time);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}

	    return d;
	  }
	  /**
		 * StrToTimestamp
		 * @描述: 把 yyyy-MM-dd HH:mm:ss 格式的时间字符串转换成timestamp 类型
		 * @作者: huangr
		 * @创建时间: 2015-4-22下午06:09:04
		 * 
		 * @修改描述: TODO 请描述修改内容
		 * @修改人: huangr
		 * @修改时间: 2015-4-22下午06:09:04
		 * @param timeString   待转换的时间string   
		 * @return   
		 *    正确的timestamp 类型
		 */
	  public static Timestamp strToTimestamp(String timeString){
		  int flag = DateTimeUtil.validateDateString(timeString);
		  if(flag==0||flag==2){
			  return null;
		  }
		  if(flag==1){
			  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
			  try {
				Date s=sdf.parse(timeString);
				long time = s.getTime();
				Timestamp p =new Timestamp(time);
				return p;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		  }
		return null;
	  }
	  /**
	 * StrToTimestamp
	 * @描述: 由正确的规定格式解析日期时间字符串，返回 Timestamp 类型  
	 * @作者: huangr
	 * @创建时间: 2015-4-7下午06:09:04
	 * 
	 * @修改描述: TODO 请描述修改内容
	 * @修改人: huangr
	 * @修改时间: 2015-4-7下午06:09:04
	 * @param timeString   待转换的时间string   
	 * @param format      待转换的时间的格式  如yyyy-MM-dd HH:mm:ss
	 * @return
	 *  TODO 描述每个输入输出参数的作用、量化单位、值域、精度
	 */
	 
	public static Timestamp strToTimestamp(String timeString,String format){
		 timeString = timeString.replaceAll("T", " "); 
		 int flag = DateTimeUtil.validateDateString(timeString);
		  if(flag==0||flag==2){
			  return null;
		  }
		  if(flag==1){
			  java.text.DateFormat sdf = new java.text.SimpleDateFormat(format);  
			  try {
				Date s=sdf.parse(timeString);
				long time = s.getTime();
				Timestamp p =new Timestamp(time);
				return p;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		  }
		return null;
	  }
	/**
	 * CalendarToTimestamp
	 * @描述: Calendar类型转换成Timestamp类型
	 * @作者: huangr
	 * @创建时间: 2015-4-8上午09:05:29
	 * 
	 * @修改描述: TODO 请描述修改内容
	 * @修改人: huangr
	 * @修改时间: 2015-4-8上午09:05:29
	 * @param calendar   
	 * @return
	 *  TODO 描述每个输入输出参数的作用、量化单位、值域、精度
	 */
	 
	public static Timestamp calendarToTimestamp(Calendar calendar){
		 Date  date= calendar.getTime();
		 Timestamp p= new Timestamp(date.getTime());
		 return p;
	  }

	  /**
	   * 生成java.util.Date类型的对象
	   * @param year int  年
	   * @param month int  月
	   * @param day int  日
	   * @return Date java.util.Date类型的对象
	   */
	  public static Date getDate(int year, int month, int day) {
	    GregorianCalendar d = new GregorianCalendar(year, month - 1, day);
	    return d.getTime();
	  }

	  /**
	   * 生成java.util.Date类型的对象
	   * @param year int  年
	   * @param month int  月
	   * @param day int  日
	   * @param hour int 小时
	   * @return Date java.util.Date对象
	   */
	  public static Date getDate(int year, int month, int day, int hour) {
	    GregorianCalendar d = new GregorianCalendar(year, month - 1, day, hour, 0);
	    return d.getTime();
	  }
	  /**
	 * getDate
	 * @描述: 生成java.util.Date类型的对象
	 * @作者: huangr
	 * @创建时间: 2015-3-30下午03:03:10
	 * 
	 * @修改描述: TODO 请描述修改内容
	 * @修改人: huangr
	 * @修改时间: 2015-3-30下午03:03:10
	 * @param year 年
	 * @param month 月
	 * @param day  日
	 * @param hour 时
	 * @param minute 分
	 * @param second 秒
	 * @return
	 *  Date java.util.Date对象
	 */
	 
	public static Date getDate(int year,int month,int day,int hour,int minute,int second){
		  GregorianCalendar d =new GregorianCalendar(year, month, day, hour, minute, second);
		  return d.getTime();
	  }

	  /**
	   * 生成圆整至小时的当前时间
	   * 例如：若当前时间为（2004-08-01 11:30:58），将获得（2004-08-01 11:00:00）的日期对象
	   * @return Date java.util.Date对象
	   */
	  public static Date getRoundedHourCurDate() {

	    Calendar cal = Calendar.getInstance();

	    cal.clear(Calendar.MINUTE);
	    cal.clear(Calendar.SECOND);
	    cal.clear(Calendar.MILLISECOND);

	    return cal.getTime();

	  }

	  /**
	   * 生成当天零时的日期对象
	   * 例如：若当前时间为（2004-08-01 11:30:58），将获得（2004-08-01 00:00:00）的日期对象
	   * @return Date java.util.Date对象
	   */
	  public static Date getRoundedDayCurDate() {
	    Calendar cal = new GregorianCalendar();

	    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                                 cal.get(Calendar.DAY_OF_MONTH)).getTime();
	  }
	  /**生成当天零时加一分的日期对象
	   * 例如：若当前时间为（2004-08-01 11:30:58），将获得（2004-08-01 00:01:00）的日期对象
	   * @return Date java.util.Date对象
	 * @return
	 */
	public static Date getRoundDayMinAddOneCurDate(){
		  Calendar cal = new GregorianCalendar();
		Date curDate =  new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                  cal.get(Calendar.DAY_OF_MONTH)).getTime();
		long oneMinDateLong = curDate.getTime() +1000*60*60;
		Date oneMinDate = new Date(oneMinDateLong);
		return oneMinDate;
	  }

	  /**
	   * 生成指定时间零时的日期对象
	   * 例如：若指定时间为（2004-08-01 11:30:58），将获得（2004-08-01 00:00:00）的日期对象
	   * @return Date java.util.Date对象
	   */
	  public static Date getRoundedDayCurDate(Date date) {
	    Calendar cal =Calendar.getInstance();
	    cal.setTime(date);
	    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                                 cal.get(Calendar.DAY_OF_MONTH)).getTime();
	  }
	  /**生成指定年份的第一天的第一秒。例如輸入2016，將獲得2016-01-01 00:00:00
	 * @param year
	 * @return
	 * @throws ParseException
	 */
	public static Date getFirstDayInYear(Integer year) throws ParseException{
		  SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   String startTimeString = year.toString()+"-01-01 00:00:00";
		   Date firstDay = sim.parse(startTimeString);
		return firstDay;
		   
	  }
	/**得到指定日期(精确到天)的第一刻，例如输入"2016-05-01"得到"2016-05-01 00:00:00"
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public static Date getFirstMoment(String time) throws ParseException{
	 SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	time = time+" 00:00:00";
	Date firstMoment = sim.parse(time);
	return firstMoment;
	}	
	/**得到指定日期(精确到天)的最后一刻，例如输入"2016-05-01"得到"2016-05-01 23:59:59"
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public static Date getLastMoment(String time) throws ParseException{
		 SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			time = time+" 23:59:59";
			Date lastMoment = sim.parse(time);
			return lastMoment;
	}
	   

	  /**
	   * 生成圆整至小时的给定时间
	   * 例如：若给定时间为（2004-08-01 11:30:58），将获得（2004-08-01 11:00:00）的日期对象
	   * @param dt Date  java.util.Date对象
	   * @return Date  java.util.Date对象
	   */
	  public static Date getRoundedHourDate(Date dt) {

	    Calendar cal = new GregorianCalendar();

	    cal.setTime(dt);

	    cal.clear(Calendar.MINUTE);
	    cal.clear(Calendar.SECOND);
	    cal.clear(Calendar.MILLISECOND);

	    return cal.getTime();
	  }

	  /**
	   * 获得给定时间的第二天零时的日期对象
	   * 例如：若给定时间为（2004-08-01 11:30:58），将获得（2004-08-02 00:00:00）的日期对象
	   *      若给定时间为（2004-08-31 11:30:58），将获得（2004-09-01 00:00:00）的日期对象
	   * @param dt Date 给定的java.util.Date对象
	   * @return Date java.util.Date对象
	   */
	  public static Date getNextDay(Date dt) {

	    Calendar cal = new GregorianCalendar();
	    cal.setTime(dt);
	    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                                 cal.get(Calendar.DAY_OF_MONTH) + 1).getTime();

	  }
	   /**获得當前时间的昨天零时的日期对象
	   * 例如：若时间为（2004-08-02 11:30:58），将获得（2004-08-01 00:00:00）的日期对象
	   *    
	 * @return
	 */
	public static Date getYesterday(){
		    Calendar cal = new GregorianCalendar();

		    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
		                                 cal.get(Calendar.DAY_OF_MONTH) - 1).getTime();
		   
	   }
	public static Date getYesterDayAddOneMin(){
		  Calendar cal = new GregorianCalendar();
			Date yesterday =  new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                  cal.get(Calendar.DAY_OF_MONTH) -1).getTime();
			long oneMinDateLong = yesterday.getTime() +1000*60*60;
			Date oneMinDate = new Date(oneMinDateLong);
		return oneMinDate;
	}
	  
	  /**   获取指定日期所在一周的指定周几的日期，
	   * 例如 指定日期 2015-3-30 00:00:00(当天为星期一),指定周5， 则返回  2015-4-3 0：00：00 (为周五).
	   * @param dt Date 给定的java.util.Date对象
	   * @param weekDay int 需要查看指定日期所在周 的周几，  就是周几的”几“，周日是7  
	   * @return Date java.util.Date对象
	   */
	  public static Date getWeekDay(Date dt,int weekDay){
	  	Calendar cal = new GregorianCalendar();
	    cal.setTime(dt);
	    if(weekDay==7)
	    	weekDay=1;
	    else
	    	weekDay++;
	    cal.set(Calendar.DAY_OF_WEEK,
	    		weekDay);
	    return cal.getTime();
	  }
	  
	  /**
	   * 获得给定时间的第N天零时的日期对象
	   * 例如：若给定时间为（2004-08-01 11:30:58），将获得（2004-08-02 00:00:00）的日期对象
	   *      若给定时间为（2004-08-31 11:30:58），将获得（2004-09-01 00:00:00）的日期对象
	   * @param dt Date 给定的java.util.Date对象
	   * @param n 
	   * @return Date java.util.Date对象
	   */
	  public static Date getNextDay(Date dt,Long n) {

	    Calendar cal = new GregorianCalendar();
	    cal.setTime(dt);

	    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                                 cal.get(Calendar.DAY_OF_MONTH) + n.intValue()).getTime();

	  }
	  /**
	   * 获得给定时间的前第N天零时的日期对象
	   * 例如：若天数为 2 给定时间为（2009-08-10 11:30:58），将获得（2009-08-08 00:00:00）的日期对象
	   * @param dt	Date 给定的java.util.Date对象
	   * @param n	天数 
	   * @return	Date java.util.Date对象
	   */
	  public static Date getLastDay(Date dt,Long n) {

		    Calendar cal = new GregorianCalendar();
		    cal.setTime(dt);

		    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
		                                 cal.get(Calendar.DAY_OF_MONTH) - n.intValue()).getTime();

	  }
	  
	  /**
	   * 获得指定时间的下个月的第n天的时间对象
	   * @param dt 时间参数
	   * @param n	天数
	   * @return Date时间对象
	   */
	  public static Date getNextMonth(Date dt,Long n) {
		  
	    Calendar cal = new GregorianCalendar();
	    cal.setTime(dt);

	    Calendar firstCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1,n.intValue());
	    return firstCal.getTime();

	  }
	  
	  
	  /**
	   * 获得两个时间相差的天数
	   * @param startDate 前一个时间
	   * @param endDate	后一个时间
	   * @return 两个时间相差的天数
	   */
	 public static long getBetweenDate(Date startDate,Date endDate){
	 	long startDateTime = startDate.getTime();
	 	long endDateTime = endDate.getTime();
	 	long dayTime = 24*60*60*1000;
	 	long days = (endDateTime-startDateTime)/dayTime;
	 	return days;
	 }
	 /**判断2个时间是否相差为1个小时，如果大于1个小时，返回true，否则返回false，
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Boolean getBetweenHourDate(Date startDate,Date endDate){
		 	Long startDateTime =  startDate.getTime();
		 	Long endDateTime = endDate.getTime();
		 	Long hours = endDateTime-startDateTime;
		 	if(hours > 1000*60*60){
		 		return true;
		 	}
		 	return false;
		 }
	 
	 /**
	 * getToNextMonthLength
	 * @描述: 获得给定时间到下一个月的天数
	 * @作者: huangr
	 * @创建时间: 2015-3-30上午10:55:58
	 * 
	 * @修改描述: 之前的实现和描述不符，现在改为与描述相符；把抛出异常给删除了
	 * @修改人: huangr
	 * @修改时间: 2015-3-30上午10:55:58
	 * @param countDate  传入的指定时间
	 * @return
	 *  Long型的  给定时间到下一个月的天数
	 */
	 
	public static long getToNextMonthLength(Date countDate){
	 	Date startDate = countDate;
	 	Date endDate = getNextMonth(startDate,new Long(1));
	 	long startDateTime = startDate.getTime();
	 	long endDateTime = endDate.getTime();
	 	long dayTime = 24*60*60*1000;
	 	long days = (endDateTime-startDateTime)/dayTime;
	 	return days;
	 }
	 /**
	  * puyuda add @2006.08.15
	  * 计算某天当月的第一天
	  * @param date 给出的时间
	  * @return 当月的第一天
	  */
	 public static Date getLatestBeforeMonth(Date date){
	 	Date latestBeforeMonth=new Date();
	 	if(date!=null){
	 		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		latestBeforeMonth=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),1).getTime();
	  	}
	 	return latestBeforeMonth;
	 }
	 /**
	  * puyuda add @2006.08.15
	  * 计算某天的前一个星期天,若给定的那天恰好是星期天，则返回当天Date
	  * @param date 给出的时间参数
	  * @return 某天的前一个星期天
	  */
	 public static Date getLatestBeforeSunday(Date date){
	 	Date latestBeforeSunday=new Date();
	 	if(date!=null){
	 		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		int startDay=cal.get(Calendar.DAY_OF_WEEK);
	  		latestBeforeSunday=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)-startDay+1).getTime();
	  	}
	 	return latestBeforeSunday;
	 }
	 /**
	  * puyuda add @2006.08.13
	  * 计算分组统计中截至月份的最后一天
	  * @param date
	  * @return 如果今天是1号，则返回上个月的上个月的最后一天，否则返回上个月的最后一天//月报表是在2号凌晨统计的
	  */
	 public static Date getFenzuEndMonth(Date date){
	 	Date fenzuStartMonth=new Date();
	 	if(date!=null){
	 		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		int startDay=cal.get(Calendar.DAY_OF_MONTH);
	  		
	  		fenzuStartMonth=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),0).getTime();
	  		if(startDay==1){
	  			fenzuStartMonth=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH)-1,0).getTime();
	  		}
	  	
	  	}
	 	return fenzuStartMonth;
	 }
	 /**
	  * puyuda add @2006.08.13
	  * 计算分组统计中截至周
	  * @param date
	  * @return 如果今天是周日和周一，则返回上一个周日，否则返回已经过去的最近的一个周日//周报表是在周二凌晨统计的
	  */
	 public static Date getFenzuEndSunday(Date date){
	 	Date sundayDate=new Date();
	 	if(date!=null){
	  		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		int dayWeek=cal.get(Calendar.DAY_OF_WEEK);
	  		if(dayWeek>2){
	  			sundayDate=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)-dayWeek+1).getTime();
	  		}else{
	  			sundayDate=new GregorianCalendar(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)-dayWeek+1-7).getTime();
	  		}
	  	}
	 	return sundayDate;
	 }
	 /**
	  * puyuda add @2006.08.12 
	  * 计算两个日期时间相差多少天,后一天比前一天前几天
	  * @param startDate 开始时间
	  * @param endDate   结束时间
	  * @return 两个日期时间相差的天数
	  */
	 public static long getDayLength(Date startDate,Date endDate){
	 	long startDateTime = startDate.getTime();
	 	long endDateTime = endDate.getTime();
	 	long dayTime = 24*60*60*1000;
	 	long days = (endDateTime-startDateTime)/dayTime;
	 	return days;
	 }
	 /**
	  * 计算所给时间的月份有多少天
	  * @param countDate 时间字串
	  * @return	当月的总天数
	  * @throws ParseException
	  */
	 public static long getMonthLength(String countDate){
	 	String firstDay = countDate.substring(0,countDate.length()-2)+"01";
	 	Date startDate=null;		
		startDate = strToDate(firstDay);		
	 	Date endDate = getNextMonth(startDate,new Long(1));
	 	long startDateTime = startDate.getTime();
	 	long endDateTime = endDate.getTime();
	 	long dayTime = 24*60*60*1000;
	 	long days = (endDateTime-startDateTime)/dayTime;
	 	return days;
	 }
	  /**
	   * 获得当前时间的第二天零时的日期对象
	   * 例如：若当前时间为（2004-08-01 11:30:58），将获得（2004-08-02 00:00:00）的日期对象
	   *      若当前时间为（2004-08-31 11:30:58），将获得（2004-09-01 00:00:00）的日期对象
	   * @return Date java.util.Date对象
	   */
	  public static Date getNextDay() {

	    Calendar cal = Calendar.getInstance();
	    return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
	                                 cal.get(Calendar.DAY_OF_MONTH) + 1).getTime();

	  }

	  /**
	   * 将java.util.Date类型的对象转换为java.sql.Timestamp类型的对象
	   * @param dt Date
	   * @return Timestamp
	   */
	  public static java.sql.Timestamp convertSqlDate(Date dt) {
	    if (dt == null) {
	      return new java.sql.Timestamp(0);
	    }
	    return new java.sql.Timestamp(dt.getTime());
	  }

	//  /**
	//   * 将java.util.Date类型的对象转换为java.sql.Timestamp类型的对象
	//   * @param dt Date
	//   * @return Timestamp
	//   */
	//  public static java.sql.Date convertDateForToSqlDate(Date dt) {
//	    if (dt == null) {
//	      return new java.sql.Date(0);
//	    }
	//
//	    return new java.sql.Date(dt.getTime());
	//  }

	  /**
	   * 格式化当前时间，返回如：2004年8月1日形式的字符串
	   * @return String
	   */
	  public static String formatCurrrentDate(String format) {
	    java.util.Date pdate = new Date();
	    return formatDate(pdate, format);
	  }

	  /**
	   * 以一定格式返回当前日期  格式为 yyyy-MM-dd HH:mm:ss 
	   * @return 格式化后的当前日期字符串
	   */
	  public static String formatCurrrentDate() {
		    java.util.Date pdate = new Date();
		    return formatDate(pdate, "yyyy-MM-dd HH:mm:ss");
	   }
	  
	  /**
	   * 按照给定格式返回代表日期的字符串
	   *
	   * @param pDate Date  要传入的日期值
	   * @param format String  日期格式
	   * @return String  代表日期的字符串
	   */
	  public static String formatDate(java.util.Date pDate, String format) {

	    if (pDate == null) {
	      pDate = new java.util.Date();
	    }
	    SimpleDateFormat sdf = new SimpleDateFormat(format);
	    return sdf.format(pDate);
	  }
	  
	  /**
	   * 按照给定格式返回代表日期的字符串，格式为  yyyy-MM-dd HH:mm:ss
	   *
	   * @param pDate Date
	   * @return String  代表日期的字符串
	   */
	  public static String formatDate(java.util.Date pDate) {
	    return formatDate(pDate, "yyyy-MM-dd HH:mm:ss");
	  }


	  /**
	   * 返回给定时间的小时数
	   * 例如：时间（2004-08-01 3:12:23）将返回 03
	   *      时间（2004-08-01 19:12:23）将返回19
	   * @param pDate Date  给定时间
	   * @return String  代表小时数的字符串
	   */
	  public static String getHour(Date pDate) {
	    return formatDate(pDate, "HH");
	  }
	  /**
	   * puyuda
	   * 判断输入的日期是不是一个月中的第一天
	   * @param date 输入的日期
	   * @return 是返回true，否则返回false
	   */
	  public static boolean isMonthStart(Date date){
	  	boolean monthStartToken=false;
	  	if(date!=null){
	  		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		if(cal.get(Calendar.DATE)==1){
	  			monthStartToken=true;
	  		}
	  	}
	  	return monthStartToken;
	  }
	  
	  /**
	   * puyuda add @ 2006.08.04
	   * 判断输入的日期是不是周一
	   * @param date 输入的日期
	   * @return 是返回true，否则返回false
	   */
	  public static boolean isWeekMonday(Date date){
	  	boolean weekMondayToken=false;
	  	if(date!=null){
	  		Calendar cal = new GregorianCalendar(Locale.CHINA);
	  		cal.setTime(date);
	  		if(cal.get(Calendar.DAY_OF_WEEK)==2){
	  			weekMondayToken=true;
	  		}
	  	}
	  	return weekMondayToken;
	  }
	  /**
	   *  puyuda add @ 2006.08.04
	   * 获得上一个月的第一天
	   * 获得上一个月的第一天
	   * @return Calendar类型的日期对象
	   */
	  public static Calendar getTheFirstDayOfTheMonth(Date date)
	  {
		  Calendar cal = new GregorianCalendar();
		  if(date==null){
		  	date=new Date();
		  }
		  cal.setTime(date);
		  cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1);
		  return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)-1,
		                                 cal.get(Calendar.DAY_OF_MONTH));

	  }
	  
	  /**
	   * puyuda add @ 2006.08.04获得上一个月的最后一天
	   * @param date 时间参数
	   * @return  Calendar类型的日期对象
	   */
	  public static Calendar getTheLastDayOfTheMonth(Date date)
	  {
		  Calendar cal = new GregorianCalendar();
		  if(date==null){
		  	date=new Date();
		  }
		  cal.setTime(date);
		  cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1);
		  System.out.println(Calendar.YEAR);
		  System.out.println(Calendar.MONTH);
		  return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
		                                 cal.get(Calendar.DAY_OF_MONTH) -1);

	  }
	  
	  /**
	   * 获得给定的年中指定月的第一天
	   * @param year 年
	   * @param month 月
	   * @return Calendar类型的日期对象
	   */
	  public static Calendar getTheFirstDayOfTheMonth(int year,int month)
	  {
		  Calendar cal = new GregorianCalendar();
		  cal.set(year,month,1);
		  return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)-1,
                  cal.get(Calendar.DAY_OF_MONTH));

	  }

	  /**
	   * 获得上一个月的最后一天
	   * @param year 年
	   * @param month 月
	   * @return Calendar类型的日期对象
	   */
	  public static Calendar getTheLastDayOfTheMonth(int year,int month)
	  {
		  Calendar cal = new GregorianCalendar();
		  cal.set(year, month, 1);
		  return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
		                                 cal.get(Calendar.DAY_OF_MONTH) -1);

	  }

	  /**
	   * 验证字符串是不是合法的日期
	   * @param dateString 待验证的日期字符串
	   * @return 满足则返回1， 不满足时，为null或者不满足为0，为“”时返回2；
	   */
	  public static int validateDateString(String dateString){
	  	
	  	if(dateString == null){
			return 0;
		} 
	  	if(dateString.equals("")){
	  		return 2;
	  	}
	  	return 1;
	/*	try {
            java.text.DateFormat.getDateInstance().parse(dateString);
            return 1;
        } catch (java.text.ParseException e) {
        	return 0;
        }*/		
	  }
	  
	  
	  /**
		 * 将Calendar类型的日期转换为String类型(毫秒)
		 * 
		 * @param calendar 日期对象
		 * @return	给定calendar的String秒值
		 * @throws Exception
		 */
		public static String formatReceiveTime(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyy-MM-dd HH:mm:ss.SSS");
				// // 去掉.uuu值为000时，去掉000
				// String uuuValue = result.substring(result.indexOf(".") + 1);
				// if (uuuValue.equals("000")) {
				// result = result.substring(0, result.indexOf("."));
				// }
				// 将日期格式变成"yyyy-MM-ddTHH:mm:ss.SSS"
				// result = result.replace(" ", "T");
				return result;
			} catch (Exception e) {
				return null;
			}
		}

		/**
		 * 将Calendar类型的日期转换为String类型(秒)
		 *  格式为 yyyy-MM-dd HH:mm:ss
		 * @param calendar 日期对象
		 * @return	给定calendar的String秒值
		 * @throws Exception
		 */
		public static String formatReceiveTimeToSec(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyy-MM-dd HH:mm:ss");

				// 将日期格式变成"yyyy-MM-ddTHH:mm:ss.SSS"
				// result = result.replace(" ", "T");
				return result;
			} catch (Exception e) {
				return null;
			}
		}

		/**
		 * 将Calendar类型的日期转换为String类型(秒)
		 * 格式为 yyyy MM dd HH:mm:ss
		 * @param calendar 日期对象
		 * @return	给定calendar的String秒值
		 * @throws Exception
		 */
		public static String formatReceiveTimeForTm(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyy-MM-dd HH:mm:ss");

				// 将日期格式变成"yyyy-MM-ddTHH:mm:ss.SSS"
				result = result.replace("-", " ");
				return result;
			} catch (Exception e) {
				return null;
			}
		}
     /**输入数字得到相应月份
     * @param month
     * @return
     */
    public static String getChineseMonth(Integer month){
    	 String chineseMonth = "";
    	 switch (month) {
		case 01: chineseMonth = "一月";
			break;
		case 02: chineseMonth = "二月";
		break;
		case 03: chineseMonth = "三月";
		break;
		case 04: chineseMonth = "四月";
		break;
		case 05: chineseMonth = "五月";
		break;
		case 6: chineseMonth = "六月";
		break;
		case 7: chineseMonth = "七月";
		break;
		case 8: chineseMonth = "八月";
		break;
		case 9: chineseMonth = "九月";
		break;
		case 10: chineseMonth = "十月";
		break;
		case 11: chineseMonth = "十一月";
		break;
		case 12: chineseMonth = "十二月";
		break;
		default:
			break;
		}
		return chineseMonth;
    	 
     }
    public  static  String getChineseQuarter(Integer month){
    	    String chineseQuarter = "";
    	switch (month) {
		case 01: chineseQuarter = "第一季度";
			break;
		case 02: chineseQuarter = "第二季度";
		break;
		case 03: chineseQuarter = "第三季度";
		break;
		case 04: chineseQuarter = "第四季度";
		break;
		default:
			break;
    	}
		return chineseQuarter;
    }
    
		/**
		 * 将Calendar类型的日期转换为String类型(秒)
		 *  格式为 yyyyMMdd HHmmssSSS
		 * @param calendar 日期对象
		 * @return	给定calendar的String秒值
		 * @throws Exception
		 */
		public static String formatCreateTimeForTm(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyyMMdd-HHmmssSSS");

				// 将日期格式变成"yyyy-MM-ddTHH:mm:ss.SSS"
				result = result.replace("-", " ");
				return result;
			} catch (Exception e) {
				return null;
			}
		}

		/**
		 * 将Calendar类型的日期转换为String类型(秒)
		 *  装换成两行数据格式，yyyy MM dd HH mm ss
		 * @param calendar 日期对象
		 * @return	给定calendar的String秒值
		 * @throws Exception
		 */
		public static String formatTimeForTwoLine(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyy-MM-dd HH:mm:ss");

				// 将日期格式变成"yyyy-MM-ddTHH:mm:ss.SSS"
				result = result.replace("-", " ");
				result = result.replace(":", " ");
				return result;
			} catch (Exception e) {
				return null;
			}
		}

		/**
		 * 
		 * 标准化为两行特定的时间串
		 * @param source 待转换的两行时间字串
		 * @return	两行特定的时间字串
		 */
		public static String formatForTwoLine(String source) {
			String result;
			String result1, result2, result3, result4, result5, result6;

			result1 = source.substring(0, 4); // yyyy

			if (source.charAt(4) == ' ' || source.charAt(4) == '-') {
				// yyyy mm dd hh:mm:ss.nnnnnn
				result2 = source.substring(5, 7); // mm
				result3 = source.substring(8, 10); // dd
				result4 = source.substring(11, 13); // hh
				result5 = source.substring(14, 16); // mm
				result6 = source.substring(17, 19); // ss
			} else {
				// yyyymmdd hh:mm:ss.nnnnnn
				result2 = source.substring(4, 6); // mm
				result3 = source.substring(6, 8); // dd
				result4 = source.substring(9, 11); // hh
				result5 = source.substring(12, 13); // mm
				result6 = source.substring(14, 16); // ss
			}

			result = result1 + " " + result2 + " " + result3 + " " + result4 + " "
					+ result5 + " " + result6;

			return result;
		}

		/**
		 * 将Calendar类型的日期(不含年)转换为String类型(秒),格式:000 00:00:00
		 * 
		 * @param calendar	日期对象
		 * @return 由Calendar转换成的String秒数 前面三个数字 表示该日期在该年的第几天
		 * @throws Exception
		 */
		public static String formatTimeForSa(Calendar calendar) {
			try {
				if (calendar == null) {
					return null;
				}
				String result = formatDate(calendar.getTime(),
						"yyyy-MM-dd HH:mm:ss");
				// 去掉年月日
				result = result.substring(10);
				// 获取日期在一年中的天数
				int day = calendar.get(Calendar.DAY_OF_YEAR);
				result = day + result;
				return result;
			} catch (Exception e) {
				return null;
			}
		}
		
		/**
		 * 将计划中的时间转换为Calendar类型 
		 * @param v 时间字符串 格式为 yyyy MM dd[T/'']HH:mm:ss.SSS
		 * @return Calendar日期对象
		 */
		public static Calendar formatStringCalendar(String v) {
			try {
				if (v == null || v.trim().length() == 0) {
					return null;
				}
				String dateStr = v;
				if (v.trim().indexOf("T") != -1) {
					dateStr = v.trim().replace("T", " ");
				}
				String dateFormat = "yyyy MM dd HH:mm:ss";
				if (dateStr.indexOf(".") != -1) {
					dateFormat += ".SSS";
				}
				Date date = strToDate(dateStr, dateFormat);
				Calendar result = Calendar.getInstance();
				result.setTime(date);
				return result;
			} catch (Exception e) {
				return null;
			}
		}
		
		/**
	     * 通过两行数据得到星历时间，返回的是  yyyy-MM-dd HH:mm:ss.SSS 格式的时间精确到毫秒
	     *
	     * @param twoLineData
	     *            两行数据
	     * @return String
	     */
	    public static String getTwoLineTime(String twoLineData) {
	        try {
	            SimpleDateFormat format = new SimpleDateFormat(
	                    "yyyy-MM-dd HH:mm:ss.SSS"); //$NON-NLS-1$
	            StringTokenizer t = new StringTokenizer(twoLineData, " "); //$NON-NLS-1$
	            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	            List<String> dataList = new ArrayList<String>();
	            while (t.hasMoreTokens()) {
	                dataList.add(t.nextToken());
	            }
	            if (dataList.size() < 1) {
	                return null;
	            } else {
	                String time = dataList.get(3);
	                int year = calendar.get(Calendar.YEAR);
	                int century = year - year % 1000;
	                calendar.set(Calendar.YEAR, century + Integer.parseInt(time.substring(0, 2)));
	                calendar.set(Calendar.DAY_OF_YEAR, Integer.parseInt(time.substring(2, 5)));
	                float timeOfDay = Float.valueOf("0" + time.substring(5)); //$NON-NLS-1$
	                int hour = (int) (timeOfDay * 24);
	                calendar.set(Calendar.HOUR_OF_DAY, hour);
	                int minute = (int) (timeOfDay * (24 * 60) - hour * 60);
	                calendar.set(Calendar.MINUTE, minute);
	                int second = (int) (timeOfDay * (24 * 60 * 60) - hour * 60 * 60 - minute * 60);
	                calendar.set(Calendar.SECOND, second);
	                int milisecond = (int) (timeOfDay * (24 * 60 * 60 * 1000) - hour * 60 * 60 * 1000 - minute * 60 * 1000 - second * 1000);
	                calendar.set(Calendar.MILLISECOND, milisecond);
//	                calendar.add(Calendar.HOUR_OF_DAY, 8);
	            }
	            format.setTimeZone(TimeZone.getTimeZone("UTC"));
	            return format.format(calendar.getTime());
	        } catch (Exception e) {
	            return null;
	        }
	    }
	    /**
		 * 获取当前时间为yyyy-MM-ddTHH:mm:ss格式的字符串
		 * 
		 * @return TimeNow
		 */
		public static String calendarToString() {
			return calendarToString(Calendar.getInstance());
		}
		
		/**
		 * 将Calendar类型转换为yyyy-MM-ddTHH:mm:ss格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String calendarToString(Calendar calendar) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String result = formatter.format(calendar.getTime());
			// 将日期格式变成"yyyy-MM-ddTHH:mm:ss"
			result = result.replace(" ", "T");
			return result;
		}

		
		/**
	     * 转化yyyy-MM-ddTHH:mm:ss.SSS到yyyy-MM-dd HH:mm:ss.SSS
	     * 原来TimeTools中的方法
	     * @param time 时间 格式为yyyy-MM-ddTHH:mm:ss.SSS
	     * @return 时间 格式为yyyy-MM-dd HH:mm:ss.SSS
	     */
	    public static String getTimeString(String time) {
	        return time.replace('T', ' ');
	    }
	    
	    /**
	     * 转化yyyy-MM-ddTHH:mm:ss.SSS到yyyy-MM-dd HH:mm:ss.SSS
	     *  原来TimeTools中的方法
	     * @param time 时间 格式为yyyy-MM-ddHH:mm:ss.SSS
	     * @return 时间 格式为yyyy-MM-ddTHH:mm:ss.SSS
	     */
	    public static String getStringTime(String time) {
	        return time.replace(' ', 'T');
	    }
	    /**
		 * 将 当前时间的Calendar类型转换为yyyyMMdd格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String dayToString() {
			return dayToString(Calendar.getInstance());
		}
		/**
		 * 将Calendar类型转换为yyyyMMdd格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String dayToString(Calendar calendar) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			String result = formatter.format(calendar.getTime());
			return result;
		}
		
		/**
		 * 将Calendar类型转换为yyyy-MM-dd格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String dayToString1(Calendar calendar) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String result = formatter.format(calendar.getTime());
			return result;
		}
		
		/**
		 * 将Calendar类型转换为HHmmss格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String timeToString(Calendar calendar) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
			String result = formatter.format(calendar.getTime());	
			return result;
		}
		
		/**
		 * 将Calendar类型转换为HH:mm:ss格式的字符串
		 * 
		 * @param calendar
		 * @return
		 */
		public static String timeToString1(Calendar calendar) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			String result = formatter.format(calendar.getTime());
			return result;
		}
		
		/**
		 * 将Calendar类型转换为format格式的字符串，若格式错误，转换为yyyy-MM-ddTHH:mm:ss格式
		 * 
		 * @param calendar 时间对象
		 * @param format  要转化的时间格式
		 * @return
		 */
		public static String calendarToString(Calendar calendar, String format) {
			if (calendar == null) {
				return "";
			}
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			try {
				return formatter.format(calendar.getTime());
			} catch (Exception e) {
				return calendarToString(calendar);
			}
		}
		
		/**
		 * 将yyyy-MM-ddTHH:mm:ss.UUU格式的字符串转换为Calendar类型
		 * 
		 * @param calendarString
		 * @return
		 */
		public static Calendar stringToCalendar(String calendarString) {
			if (calendarString == null || calendarString.trim().length() == 0) {
				return null;
			}
			String dateStr = calendarString;
			if (calendarString.trim().indexOf("T") != -1) {
				dateStr = calendarString.trim().replace("T", " ");
			}
			String dateFormat = "yyyy-MM-dd HH:mm:ss";
			if (dateStr.indexOf(".") != -1) {
				dateFormat += ".SSS";
			}
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(formatter.parse(dateStr));
			} catch (ParseException e) {
				return null;
			}
			return calendar;
		}
		
		
		
		/**
		 * 将yyyy-MM-dd格式的字符串转换为Calendar类型
		 * 
		 * @param calendarString
		 * @return
		 */
		public static Calendar stringToCalendar1(String calendarString) {
			if (calendarString == null || calendarString.trim().length() == 0) {
				return null;
			}
			String dateStr = calendarString;
			String dateFormat = "yyyy-MM-dd";
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(formatter.parse(dateStr));
			} catch (ParseException e) {
				return null;
			}
			return calendar;
		}
		
		
		/**
		 * 
		 * 将一定的格式的时间字符串以一定的字符串格式解析为Calendar类型
		 * 如 2077-12-2T12:23:45 用yyyy-MM-dd HH:mm:ss 解析,
		 * 结果将会是2017-12-2 12:23:45 这个时间所对应的对象
		 * @param calendarString   
		 * @param format   要转化格式
		 * @return
		 */
		public static Calendar stringToCalendar(String calendarString, String format) {
			if (calendarString == null || calendarString.trim().length() == 0) {
				return null;
			}
			String dateStr = calendarString;
			if (calendarString.trim().indexOf("T") != -1) {
				dateStr = calendarString.trim().replace("T", " ");
			}
			String dateFormat = format;
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(formatter.parse(dateStr));
			} catch (ParseException e) {
				return null;
			}
			return calendar;
		}
		
		/**
		 * 将calendar中的T替换为空格，如 yyyy-MM-ddTHH:mm:ss 将变为   yyyy-MM-dd HH:mm:ss 
		 * 
		 * @param calendar
		 * @return
		 */
		public static String formatCalendar(String calendar) {
			if (calendar == null) {
				return "";
			}
			return calendar.replace("T", " ");
		}
		
		/**
		 * 检查日期格式是否正确，格式为 yyyy-MM-dd
		 * 
		 * @param day
		 * @return
		 */
		public static boolean checkDayFormat(String day) {
			if (day == null||day=="") {
				return false;
			} else {
				String dateFormat = "yyyy-MM-dd";
				SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
				try {
					formatter.parse(day);
					return true;
				} catch (ParseException e) {
					return false;
				}
			}
		}
		
		/**
		 * 检查时间格式是否正确，时间格式为 HH:mm:ss
		 * 
		 * @param time
		 * @return
		 */
		public static boolean checkTimeFormat(String time) {
			if (time == null) {
				return false;
			} else {
				String dateFormat = "HH:mm:ss";
				SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
				try {
					formatter.parse(time);
					return true;
				} catch (ParseException e) {
					return false;
				}
			}
		}
		
		/**
		 * currentTime
		 * @描述: 返回自 1970 年 1 月 1 日 00:00:00 GMT 以来此 Date 对象表示的毫秒数
		 * @作者: huangr
		 * @创建时间: 2015-3-27上午09:26:41
		 * 
		 * @修改描述: TODO 请描述修改内容
		 * @修改人: huangr
		 * @修改时间: 2015-3-27上午09:26:41
		 * @return   毫秒数
		 *  
		 */
		 
		public static long currentTime(){
			return Calendar.getInstance().getTime().getTime();
		}
		
		/**
		 * getDBtime
		 * @描述: 获取当前的util  对象
		 * @作者: huangr
		 * @创建时间: 2015-3-27上午09:35:05
		 * 
		 * @修改描述: TODO 请描述修改内容
		 * @修改人: huangr
		 * @修改时间: 2015-3-27上午09:35:05
		 * @return
		 *  
		 */
		public static java.util.Date getDBtime(){
			//return new java.util.Date(currentTime());
			return new java.util.Date();
		}
		public static java.sql.Timestamp getTimestamp(){
			return new java.sql.Timestamp(System.currentTimeMillis());
		}
		
		public static boolean testTimeFormat(String time){
			return Pattern.matches("[1-2]\\d{3}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-6][0-9]:[0-6][0-9]", time);
		}

		
		/**
		 * requestTimeFormat
		 * @描述: 格式化时间格式。如果为空或者时间格式不对，则用默认值
		 * @作者: 梁家文
		 * @创建时间: 2015-5-5上午11:14:06
		 * @param time 时间字符串
		 * @param type 起始时间或者结束时间
		 * @return string
		 */
		 
		public static Timestamp requestTimeFormat(String time,String type){
			if(type.equalsIgnoreCase("s")){
				if(StringUtils.isBlank(time)||!testTimeFormat(time)) time = "1970-01-01 00:00:00";
			}else if(type.equalsIgnoreCase("e")){
				if(StringUtils.isBlank(time)||!testTimeFormat(time)) return getTimestamp();
			}else return null;
			return strToTimestamp(time);
		}
		/**
		 * formatTime
		 * @描述: 获取指定格式的时间字符串
		 * @作者: 梁家文
		 * @创建时间: 2015-5-7下午05:00:41
		 * @param expression  如yyyyMMddHHmmss
		 * @return 指定格式的时间字符串
		 */
		 
		public static String formatTime(String expression) {
			return (new SimpleDateFormat(expression)).format(Calendar.getInstance().getTime());
		}
		
		/**
		 * getCurrentTime
		 * @描述: 得到当前时间的时间字符串
		 * @作者: 曾贵荣
		 * @创建时间: 2015-12-21下午08:10:21
		 * @param format 时间格式
		 * @return
		 *      时间字符串
		 */
		public static String getCurrentTime(String format){
			Date date = new Date();
			return formatDate(date, format);
		}
		
		/**
		 * getEndTime
		 * @描述: 根据开始时间和时长，得到结束时间
		 * @作者: 曾贵荣
		 * @创建时间: 2015-12-14下午08:40:11
		 * @param startTime 开始时间 "yyyy-MM-dd HH:mm:ss"
		 * @param duration 长度
		 * @param field 增加的时间类型，天/小时/其他 如Calendar.Date
		 * @return
		 *       成功则返回String类型的结束时间 ， 格式：yyyy-MM-dd HH:mm:ss，否则返回null
		 */
		public static String getEndTime(String startTime,Integer duration,int field){
			if(startTime==null||duration==null||duration<1){
				return null;
			}
			Calendar calendar = DateTimeUtil.stringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			if(calendar==null){
				return null;
			}
			calendar.add(field, duration);
			startTime = DateTimeUtil.calendarToString(calendar,"yyyy-MM-dd HH:mm:ss");
			return startTime;
		}
		
		/**
		 * getMillisecond
		 * @描述: 转换为毫秒级的时间格式
		 * @作者: 肖建平
		 * @创建时间: 2016-7-18下午02:56:42
		 * @param mDate 格式为：yyyy-MM-dd HH:mm:ss
		 * @return
		 *  输出格式为：yyyy-MM-dd HH:mm:ss.SSS
		 */
		 
		public static String getMillisecond(String mDate){
			SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date dt = null;
			String st="";
			try {
				dt = sdf.parse(mDate);
				st = formatDate(dt, "yyyy-MM-dd HH:mm:ss.SSS");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return st;
		}
}

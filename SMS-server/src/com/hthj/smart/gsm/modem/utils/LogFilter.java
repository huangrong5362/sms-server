package com.hthj.smart.gsm.modem.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;



/**
 * @类名: LogFilter
 * @描述: 过滤器，用于获取用户名和IP地址
 * @版本: 
 * @创建日期: 2015-6-25下午01:32:52
 * @作者: 梁家文
 * @JDK: 1.6
 */
public class LogFilter implements Filter{

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse rep, FilterChain chain) throws IOException, ServletException {
		String address = req.getRemoteAddr();
		Object userName = ((HttpServletRequest)req).getSession().getAttribute("userName");
		MDC.put("address", address);
		MDC.put("userName", userName==null?"unknown":userName);
		chain.doFilter(req, rep);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}

package com.hthj.smart.gsm.modem.activitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.smslib.AGateway;
import org.smslib.GatewayException;
import org.smslib.InboundMessage;
import org.smslib.Message;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.AGateway.Protocols;
import org.smslib.Message.MessageEncodings;
import org.smslib.modem.SerialModemGateway;
import org.springframework.stereotype.Component;

import com.hthj.smart.gsm.modem.config.Config;

/**
 * 短信发送接收实现类
 * 
 * @author 
 */

public class SmsHandler {
	private static final Logger logger = Logger.getLogger(SmsHandler.class);
	public final static String MSG_FILE_PATH = "server.properties";
	private static final SmsHandler instance = new SmsHandler();

	private SmsHandler() {
	};

	public static SmsHandler getInstance() {
		return instance;
	}

	private String id = Config.id;				//短信猫端口号
	private String comPort = Config.comPort;	//串口名称
	private String baudRate = Config.baudRate;	//串口速率
	private String manufacturer = Config.manufacturer;	//生产厂商
	private String model = Config.model;	//设备型号
	private String simPin = "0000";		//SIM卡锁
	public Service smsService;
	private List<AGateway> agatewayList = new ArrayList<AGateway>();

	/**
	 * 初始化端口配置信息
	 */
	public void init() {

		try {
			smsService = Service.getInstance();

			logger.info("..............init services...params.............");
			logger.info("id:" + Config.id);
			logger.info("comPort:" + Config.comPort);
			logger.info("baudRate:" + Config.baudRate);
			logger.info("manufacturer:" + Config.manufacturer);
			logger.info("model:" + Config.model);
			String[] ids = id.split(",");
			String[] comPorts = comPort.split(",");
			String[] baudRates = baudRate.split(",");
			// String[] manufacturers=manufacturer.split(",");
			// String[] models=model.split(",");
			for (int i = 0; i < comPorts.length; i++) {
				int baudrate = Integer.parseInt(baudRates[i]);
				SerialModemGateway gateway = new SerialModemGateway(ids[i], comPorts[i], baudrate, manufacturer, "");

				gateway.setInbound(true);		//设置true，表示该网关可以接收短信,根据需求修改
				gateway.setOutbound(true);		//设置true，表示该网关可以发送短信,根据需求修改
				gateway.setProtocol(Protocols.PDU);
				gateway.setSimPin(simPin);		//sim卡锁，一般默认为0000或1234
				agatewayList.add(gateway);		//将网关添加到短信猫服务中
			}

			try {
				for (AGateway gatewayTmp : agatewayList) {
					smsService.addGateway(gatewayTmp);
				}
			} catch (GatewayException ex) {
				logger.error("addGateWay失败！" + ex.getMessage());
			}

		} catch (Exception e) {
			logger.error(" 初始化端口配置信息失败！" + e.getMessage());
		}
	}

	/**
	 * start service启动服务
	 */
	public void start() {
		logger.info("SMS service start.....");
		try {
			// init();
			smsService.getInstance().S.SERIAL_POLLING = true; // 启用轮循模式
			smsService.startService();
			logger.info("SMS service start sucess");
		} catch (Exception ex) {
			logger.error("SMS service start error:", ex);
		}

	}
	

	/**
	 * isStarted 检测服务是否启动
	 * 
	 * @return
	 */
	public synchronized boolean isStarted() {
		if (Service.getInstance().getServiceStatus() == Service.ServiceStatus.STARTED) {
			if (SmsHandler.getInstance() == null)
				
				return false;
			for (AGateway gateway : smsService.getGateways()) {
				if (gateway.getStatus() == AGateway.GatewayStatuses.STARTED) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * sycInit 如果服务未启动，则启动服务
	 */
	public synchronized void sycInit() {
		if (isStarted()) {
			return;
		}
		
		init();
		start();
		logger.info("服务已启动");
	}

	/**
	 * 注销服务
	 */
	public synchronized void destroy() {
		try {
			smsService.stopService();
			for (AGateway gateway : agatewayList) {
				logger.info(".................gateway.getStatus():" + gateway.getStatus());
				smsService.removeGateway(gateway);
			}
			agatewayList.clear();
			System.out.println(".........agatewayList.size()" + agatewayList.size() + "............");
			logger.info("SMS service stop sucess");
		} catch (Exception ex) {
			logger.error("SMS service stop error:", ex);
		}
	}

	/**
	 * send SMS 发送短信
	 * 
	 * @param msg
	 * @return Boolean
	 */
	public Boolean sendSMS(OutboundMessage msg) {
		try {
			msg.setEncoding(MessageEncodings.ENCUCS2);
			return smsService.sendMessage(msg);
		} catch (Exception e) {
			logger.error("send error:", e);
			return false;
		}
	}

	/**
	 * read SMS 接收短信
	 * 
	 * @return List
	 */
	public List<InboundMessage> readSMS() {
		List<InboundMessage> msgList = new LinkedList<InboundMessage>();
		if (!isStarted()) {
			return msgList;
		}
		try {
			this.smsService.readMessages(msgList, InboundMessage.MessageClasses.ALL);
			logger.info("read SMS size: " + msgList.size());
		} catch (Exception e) {
			logger.error("read error:", e);
		}
		return msgList;
	}

	/**
	 *
	 * send 发送消息返回结果
	 *
	 * @param mobile 手机号
	 * @param content 发送内容
	 * @return
	 * 
	 */
	public synchronized boolean send(String mobile, String content) {
		try {
			// sycInit();
			OutboundMessage msg = new OutboundMessage(mobile, content);
			msg.setEncoding(MessageEncodings.ENCUCS2);
			boolean result = Service.getInstance().sendMessage(msg);
			System.out.println(msg);
			return result;
		} catch (Exception e) {
			logger.error("send error:", e);
			return false;
		}
	}

	/**
	 * 读取短信
	 */
	public synchronized List<Message> read() {
		List<Message> resultList = new ArrayList<Message>();
		List<InboundMessage> msgList = new LinkedList<InboundMessage>();
		if (!isStarted()) {
			return resultList;
		}
		try {
			Service.getInstance().readMessages(msgList, InboundMessage.MessageClasses.ALL);
			logger.info("read SMS size: " + msgList.size());
			Message message = null;
			for (InboundMessage msg : msgList) {
				System.out.println(msg);
				// message = new Message();
				if (msg.getDate() == null) {
					msg.setDate(new Date());
				}
				org.apache.commons.beanutils.BeanUtils.copyProperties(message, msg);
				Service.getInstance().deleteMessage(msg);
				resultList.add(message);
			}
		} catch (Exception e) {
			logger.error("read error:", e);
		}
		return resultList;
	}

	/**
	 * 测试
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < 3; i++) {
			Logger.getRootLogger().setLevel(Level.INFO);
			OutboundMessage outMsg = new OutboundMessage("+8618060476891", "正在测试短信多条发送的稳定性。");
			SmsHandler.getInstance().sycInit();
			SmsHandler smsHandler = new SmsHandler();
			// 发送短信
			System.out.println("result:" + smsHandler.sendSMS(outMsg));
			;
			System.out.println("result:" + smsHandler.send("18060476891", "正在测试短信多条发送的稳定性。"));
			;
			// 读取短信
			List<Message> readList = SmsHandler.getInstance().read();
			// List<InboundMessage> readList1 = smsHandler.readSMS();

			for (Message in : readList) {
				System.out.println("发信人：" + ((InboundMessage) in).getOriginator() + " 短信内容:" + in.getText());
			}
			SmsHandler.getInstance().destroy();
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComPort() {
		return comPort;
	}

	public void setComPort(String comPort) {
		this.comPort = comPort;
	}

	public String getBaudRate() {
		return baudRate;
	}

	public void setBaudRate(String baudRate) {
		this.baudRate = baudRate;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSimPin() {
		return simPin;
	}

	public void setSimPin(String simPin) {
		this.simPin = simPin;
	}

}
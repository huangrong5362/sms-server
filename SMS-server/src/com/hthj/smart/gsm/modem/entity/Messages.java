package com.hthj.smart.gsm.modem.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Massages")
public class Messages {

	/**
	 * serialVersionUID:序列化时为了保持版本的兼容性，即在版本升级时反序列化仍保持对象的唯一性
	 */
	private static final long serialVersionUID = 1401641006472512835L;

	/**
	 * id：主键
	 */
	@Id
	@GenericGenerator(name = "id", strategy = "assigned")
	@Column(length = 50)
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	/**
	 * appCode:应用ID
	 */
	@Column(length = 32)
	private String appCode;

	/**
	 * phones:手机号
	 */
	@Column(length = 20)
	private String phones;

	/**
	 * contents:短信内容
	 */
	@Column(length = 255)
	private String content;

	/**
	 * IsMult:是否群发
	 */
	@Column(length = 10)
	private Boolean group;

	public Boolean getGroup() {
		return group;
	}

	public void setGroup(Boolean group) {
		this.group = group;
	}

	/**
	 * time:发送/接收时间
	 */
	@Column(length = 32)
	private Date time;

	/**
	 * result:反馈结果
	 */
	@Column(length = 255)
	private Boolean result;

}

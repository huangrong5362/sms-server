package com.hthj.smart.gsm.modem.config;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.hthj.smart.gsm.modem.utils.PropertiesReader;

public class Config {
	
	private static File file=null;
//	private static FileInputStream is =null;
	private static OutputStream os=null;
	
	private static Logger logger=Logger.getLogger(Config.class);
	public static String id;

	
	public static String comPort;
	public static List<String> appCodes;

	public static String baudRate;
	public static String manufacturer;
	public static String model;
	public static String simPin;
	static{
		 file =new File(Config.class.getResource("/").getPath(),"server.properties");
		update();
		
	}
	public static void update(){
		PropertiesReader reader=null;
		try {
			reader = new PropertiesReader(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}
		appCodes=new ArrayList<String>();
		String[] appcs=reader.get("appCode").split(",");
		for(String app :appcs){
			appCodes.add(app);
			
		}
		
		 id = reader.get("send.gatewayId");  
		   comPort = reader.get("send.comPort");  
		   baudRate =reader.get(("send.baudRate"));  
		   manufacturer = reader.get("modem.manufacturer");  
		   model = reader.get("modem.model")==null?"":reader.get("modem.model");  
		   simPin = reader.get("modem.simPin");  
		
	/*	
		appCodes =new ArrayList<String>();*/
	/*	String[] appcs=reader.get("modem.appCodes").split(",");*/
		/*for(String app :appcs){
			appCodes.add(app);
		}*/

	}
	public static boolean setConfig(Map<String,Object> config){
		Properties pro =new Properties();
	/*	String nHOST=(String) config.get("HOST");
		String nPORT=(String) config.get("PORT");
		String nautoModule=(String) config.get("autoModule");
		String nmoduleType=(String) config.get("moduleType");
		String nworkType=(String) config.get("workType");
		String ntimeOut=(String) config.get("timeOut");
		String nmsgFormat= (String) config.get("msgFormat");
		String nrountType=  (String) config.get("rountType");
		String nappCodes =(String) config.get("appCodes");*/
		try {
			pro.load(new FileInputStream(file));
			os =new FileOutputStream(file);
			
		} catch (FileNotFoundException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}
		try {
		/*	if(nHOST!=null){
				pro.setProperty("modem.host", nHOST);
			}
			if(nPORT!=null){
				pro.setProperty("modem.port", nPORT);
			}
			if(nautoModule!=null){
				pro.setProperty("modem.autoModule", nautoModule.toString());
			}
			if(nmoduleType!=null){
				pro.setProperty("modem.moduleType", nmoduleType.toString());
			}
			if(nworkType!=null){
				pro.setProperty("modem.workType", nworkType.toString());
			}
			if(ntimeOut!=null){
				pro.setProperty("modem.timeOut", ntimeOut.toString());
			}
			if(nmsgFormat!=null){
				pro.setProperty("modem.msgFormat", nmsgFormat.toString());
			}
			if(nrountType!=null){
				pro.setProperty("modem.rountType", nrountType.toString());
			}
			if(nappCodes!=null){
				pro.setProperty("modem.appCodes", nappCodes);
			}*/
			//保存一下
			pro.store(os,null);
			os.close();
			update();  //刷新一下内存
			logger.info("update config.properties success!");
			System.out.println("update config.properties success!");
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static void main(String[]args){
		File n =new File(Config.class.getResource("/").getPath(),"modem.properties");
		System.out.println(n.getAbsolutePath());
		Properties pro =new Properties();
		try {
			pro.load(new FileInputStream(n));
			String a =pro.getProperty("comPort");
			System.out.println(a);
		} catch (FileNotFoundException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}
		FileOutputStream os =null;
		/*try {
			os=new FileOutputStream(n);
			pro.setProperty("PORT", "3456");
			pro.store(os, null);
			os.close();
		} catch (IOException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}
		*/
		
	}
}

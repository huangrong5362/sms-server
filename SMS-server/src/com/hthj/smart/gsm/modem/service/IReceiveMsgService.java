package com.hthj.smart.gsm.modem.service;

import java.util.List;

import com.hthj.smart.gsm.modem.entity.ReceiveMsg;

/**
 * 短信操作类接口
 * 
 * @author wudi
 *
 */
public interface IReceiveMsgService {

	public Boolean save(ReceiveMsg rmsg);
	
	public Boolean delete(List<String> ids);
	
	public Boolean update(ReceiveMsg rmsg);
	
	public List<ReceiveMsg> queryAll();
	
	public List<ReceiveMsg> queryBy(String phone,String key,String startTime,String endTime);
	
	public ReceiveMsg queryById(String id);
}

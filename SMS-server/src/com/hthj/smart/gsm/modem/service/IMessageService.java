package com.hthj.smart.gsm.modem.service;

import java.util.List;

import com.hthj.smart.gsm.modem.entity.Messages;

/**
 * 短信操作类接口
 * 
 * @author wudi
 *
 */
public interface IMessageService {

	public Boolean save(Messages message);
	
	public Boolean delete(List<String> ids);
	
	public Boolean update(Messages message);
	
	public List<Messages> queryAll();
	
	public List<Messages> queryBy(String appCode,String phones,String key,String startTime,String endTime);
	
	public Messages queryById(String id);
}

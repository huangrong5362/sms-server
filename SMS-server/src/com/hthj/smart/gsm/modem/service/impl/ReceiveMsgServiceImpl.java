package com.hthj.smart.gsm.modem.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.hthj.smart.gsm.modem.dao.ReceiveMsgDAO;
import com.hthj.smart.gsm.modem.entity.ReceiveMsg;
import com.hthj.smart.gsm.modem.service.IReceiveMsgService;
import com.hthj.smart.gsm.modem.utils.DateTimeUtil;
@Service(value = "receiveMsgService")
@Component
public class ReceiveMsgServiceImpl implements IReceiveMsgService{

	private static Logger logger = Logger.getLogger("com/hthj/smart/gsm/modem/service/impl/MessageServiceImpl");
	@Autowired
	private ReceiveMsgDAO receiveMsgDAO;

	public void setReceiveMsgDAO(ReceiveMsgDAO receiveMsgDAO) {
		this.receiveMsgDAO = receiveMsgDAO;
	}

	@Override
	public Boolean save(ReceiveMsg rmsg) {
		try {
			rmsg.setTime(new Date());
			receiveMsgDAO.save(rmsg);
			logger.info("save rmsg success!" + rmsg.toString());
		} catch (Exception e) {
			logger.error("save rmsg fail." + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public Boolean delete(List<String> ids) {
		for(String i:ids){
			try {
				ReceiveMsg msg = receiveMsgDAO.findOne(i);
				if(msg==null){
					continue;
				}
				receiveMsgDAO.delete(msg);
				logger.warn("delete message succcess:" + msg.toString());
			} catch (Exception e) {
				logger.error("delete receiveMsg fail." + e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@Override
	public Boolean update(ReceiveMsg messages) {
		ReceiveMsg now =receiveMsgDAO.findOne(messages.getId());
		if(now==null){
			logger.warn("update message("+messages.getPhones()+messages.getTime()+") failed.messages not exist!");
			return false;
		}
		messages.setTime(now.getTime()); //这个时间不要去更新
		try {
			receiveMsgDAO.saveAndFlush(messages);
			logger.info("update message("+messages.getPhones()+messages.getTime()+") success!" );
		} catch (Exception e) {
			logger.error("update messages faild." + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<ReceiveMsg> queryAll() {
		return receiveMsgDAO.findAll();
	}

	@Override
	public List<ReceiveMsg> queryBy(String phones, String key,String startTime,String endTime) {
		if (StringUtils.isBlank(phones)) {
			phones = null;
		} else {
			phones = "%" + phones + "%";
		}
		if(StringUtils.isBlank(key)){
			key = null;
		}else{
			key = "%" + key + "%";
		}
		if (StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)) {
			return receiveMsgDAO.findBy(phones, key);
		}
		Date st = DateTimeUtil.strToDate(startTime,"yyyy-MM-dd hh:MM:ss");
		Date et = DateTimeUtil.strToDate(endTime,"yyyy-MM-dd hh:MM:ss");
		return receiveMsgDAO.findByKey(phones, key,st,et);
	}

	@Override
	public ReceiveMsg queryById(String id) {
		return receiveMsgDAO.findOne(id);
	}

}

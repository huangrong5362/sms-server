package com.hthj.smart.gsm.modem.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hthj.smart.gsm.modem.dao.MessagesDAO;
import com.hthj.smart.gsm.modem.entity.Messages;
import com.hthj.smart.gsm.modem.service.IMessageService;
import com.hthj.smart.gsm.modem.utils.DateTimeUtil;
@Service(value = "messageService")
@Component
public class MessageServiceImpl implements IMessageService{

	private static Logger logger = Logger.getLogger("com/hthj/smart/gsm/modem/service/impl/MessageServiceImpl");
	@Autowired
	private MessagesDAO messagesDAO;
	public void setMessagesDAO(MessagesDAO messagesDAO) {
		this.messagesDAO = messagesDAO;
	}
	@Override
	public Boolean save(Messages message) {
		try {
			message.setTime(new Date());
			messagesDAO.save(message);
			logger.info("save message success!" + message.toString());
		} catch (Exception e) {
			logger.error("save message fail." + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional
	@Override
	public Boolean delete(List<String> ids) {
		for(String i:ids){
			try {
				Messages msg = messagesDAO.findOne(i);
				if(msg==null){
					continue;	
				}
				messagesDAO.delete(msg);
				logger.warn("delete message succcess:" + msg.toString());
				
			} catch (Exception e) {
				logger.error("delete message fail." + e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@Override
	public Boolean update(Messages messages) {
		Messages now =messagesDAO.findOne(messages.getId());
		if(now==null){
			logger.warn("update message("+messages.getAppCode()+messages.getTime()+") failed.messages not exist!");
			return false;
		}
		messages.setTime(now.getTime()); //这个时间不要去更新
		try {
			messagesDAO.saveAndFlush(messages);
			logger.info("update messages("+messages.getAppCode()+messages.getTime()+") success!" );
		} catch (Exception e) {
			logger.error("update messages faild." + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<Messages> queryAll() {
		return messagesDAO.findAll();
	}

	@Override
	public List<Messages> queryBy(String appCode, String phones, String key,String startTime,String endTime) {
		if (StringUtils.isBlank(appCode)) {
			appCode = null;
		} 
		if (StringUtils.isBlank(phones)) {
			phones = null;
		} else {
			phones = "%" + phones + "%";
		}
		if(StringUtils.isBlank(key)){
			key = null;
		}else{
			key = "%" + key + "%";
		}
		if (StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)) {
			return messagesDAO.findBy(appCode, phones, key);
		}
		Date st = DateTimeUtil.strToDate(startTime,"yyyy-MM-dd hh:MM:ss");
		Date et = DateTimeUtil.strToDate(endTime,"yyyy-MM-dd hh:MM:ss");
		return messagesDAO.findByKey(appCode, phones, key,st,et);
	}

	@Override
	public Messages queryById(String id) {
		return messagesDAO.findOne(id);
	}

}
